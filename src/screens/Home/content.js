// NOTES: ALL DATA ARE HARDCODED AND
// PARSED TO ELEMENTS AS PROPS
//
// See /src/data for more details

import HeaderItems from '../../data/HeaderItems';
import SocialLinksItems from '../../data/SocialLinks';
import ProductsItems from '../../data/NewestProductsItems';
import BigBannerItem from '../../data/BigBannerItem';
import NewestPostsItems from '../../data/NewestPostsItems';
import SponsorItems from '../../data/SponsorItems';

// the main content
export default function Content(props) {
    return (
        <div className="container">
            <HeaderSlider items={HeaderItems} />
            <SocialLinks items={SocialLinksItems} />
            <NewestProducts items={ProductsItems} />
            <BigBanner item={BigBannerItem} />
            <NewestPosts items={NewestPostsItems} />
            <SponsorSection items={SponsorItems} />
        </div>
    )
}
//
// The header slider on the top of the page
//
// See /src/data/HeaderItems.js for more info of data structure
function HeaderSlider(props) {
    let items = props.items,
        carouselItems = [], tabChanger = [];

    if (Array.isArray(items)) {
        for (let idx in items) {
            let item = items[idx];
            carouselItems.push(HeaderItem({ ...item, key: idx }));
            tabChanger.push(<li key={"tab-" + idx} data-target="#carouselExampleIndicators" data-slide-to="0" className="active"></li>);
        }
    }

    return (
        <div id="carouselExampleIndicators" className="carousel slide" data-ride="carousel">
            <style jsx>{`
                @media(max-width: 576px){  
                    .carousel-indicators{
                        display: none;
                    }
                }
            `}</style>
            <ol className="carousel-indicators">
                <li data-target="#carouselExampleIndicators" data-slide-to="0" className="active"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
            </ol>
            <div className="carousel-inner">
                {carouselItems}
            </div>
            <a className="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                <span className="carousel-control-prev-icon" aria-hidden="true"></span>
                <span className="sr-only">Previous</span>
            </a>
            <a className="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                <span className="carousel-control-next-icon" aria-hidden="true"></span>
                <span className="sr-only">Next</span>
            </a>
        </div>
    )

}
// The slider item which contains 
// a big image, a title and a call-to-action button
//
// Note: The call-to-action button will not displayed
// if the `goto` property is an empty string ""
function HeaderItem(props) {
    const titleLimitLength = 40;
    return (
        <div key={props.key} className={"carousel-item" + ((props && props.key == "0") ? " active" : "")}>
            <style jsx>{`
                img {
                    width: 100%;
                    opacity: 0.7;
                    max-height: 500px;
                }
                .carousel-content {
                    text-align: right;
                    position: absolute;
                    right: 0;
                    bottom: 20%;
                    padding-right: 15%;
                    width: 60% !important;
                }
                h1 {
                    font-weight: bold;
                    text-transform: uppercase;
                }
                a {
                    margin-top: 10px !important;
                    padding: 10px 0px 10px 0px;
                    width: 150px;
                    background: #2EADED !important;
                    border: 1px solid #2EADED;
                    color: white;
                }
                
                @media(max-width: 576px){
                    h1 {
                        font-size: 4vw !important;
                    }
                    a {
                        margin-top: 0px !important;
                        font-size: 0.6em;
                        zoom: 0.8;
                    }
                }
                @media(min-width: 576px){
                    h1 {
                        font-size: 1.5em !important;
                    }
                }
                @media(min-width: 768px){
                    h1 {
                        font-size: 2em !important;
                    }
                }
                @media(min-width: 992px){
                    h1 {
                        font-size: 3em !important;
                    }
                }
                @media(min-width: 1200px){
                    h1 {
                        font-size: 3.5em !important;
                    }
                }
            `}</style>
            <img className="d-block w-100" src={props.imgUrl} alt={props.title} />
            <div className="carousel-content">
                <h1 className="d-block w-100">{props.title.length <= titleLimitLength ? props.title : props.title.slice(0, titleLimitLength) + "..."}</h1>
                {props.goto && props.goto.length > 0 ? <a className="btn btn-primary" href={props.goto}>TÌM HIỂU</a> : <></>}
            </div>
        </div>
    )
}
// 
// The section under the carousel slider
// normally contains 3 elements
//
// See /src/data/SocialLinks.js for more info of data structure
function SocialLinks(props) {
    let items = props.items,
        socialItems = [];

    for (let idx in items) {
        let item = items[idx];
        socialItems.push(SocialItem(item))
    }
    return (
        <>
            <style jsx>{`
                div {
                    margin-top: 30px;
                }
            `}
            </style>
            <div className="row">
                {socialItems}
            </div>
        </>
    )
}
// a link item in the SocialLinks section
// should contains a link displayed as an image
function SocialItem(props) {
    return (
        <div key={props.id} className="col-12 col-sm-4">

            <style jsx>{`
            div {
                position: relative;
                margin-bottom: 100px;
            }
            img {
                transition: 0.5s;
                max-height: 250px;
            }
            div:hover img{
                opacity: 0.15;
                -webkit-transform: rotate(-2deg);
                -moz-transform: rotate(-2deg);
                -o-transform: rotate(-2deg);
            }
            p {
                display: none;
                color: #2EADED;
                font-size: 1.4em;
                position: absolute;
                left: 0;
                top: 50%;
            }
            div:hover p {
                display: block;
                position: absolute;
                top: 50%;
                left: 50%;
                transform: translate(-50%, -50%);
            }
            img {
                width: 100%;
            }
        `}
            </style>
            <a href={props.url}>
                <img src={props.imgUrl} alt={props.description} />
                <p>{props.description}</p>
            </a>
        </div>

    )
}
// 
// The Newest Products Section
// should contains products which was a multiple of 4
//
// See /src/data/NewestProductsItems.js for more info of data structure
function NewestProducts(props) {
    let items = props.items,
        productItems = [];

    for (let idx in items) {
        let item = items[idx];
        productItems.push(Product(item));
    }

    return (
        <>
            <style jsx>{`
                div {
                    text-align: center;
                    margin-bottom: 100px;
                }    
                h1 {
                    font-weight: bold;
                }
                @media(max-width: 576px){  
                    h1{
                        font-size: 1.5em;
                    }
                }
            `}</style>
            <div>
                <h1>SẢN PHẨM MỚI NHẤT</h1>
                <p>Những sản phẩm đặc sắc mới được bình chọn</p><br />
                <div className="row">
                    {productItems}
                </div>
            </div>
        </>
    )
}
//
function Product(props) {
    return (
        <div key={props.id} className="col-12 col-sm-6 col-md-6 col-lg-3">
            <style jsx>{`
                a:hover .productImgContainer {
                    box-shadow:
                        1px 1px #53a7ea,
                        2px 2px #53a7ea,
                        3px 3px #53a7ea;
                    -webkit-transform: translateX(-3px);
                    transform: translateX(-3px);
                }
                div.productImgContainer {
                    height: 300px;
                    width: 100%;
                    margin-bottom: 20px;
                    vertical-align: middle;
                    background: #d5f1ff;
                    display: inline-block;
                    vertical-align: top; /*not required*/
                    position: relative;
                    transition: 0.8s;
                }
                span {
                    display: inline-block;
                    height: 100%;
                    vertical-align: middle;
                }
                img {
                    width: 100%;
                    max-height: 100%;
                    position: absolute;
                    top: 0;
                    bottom: 0;
                    left: 0;
                    right: 0;
                    margin: auto;
                    transition: 2s;
                }
                h5 {
                    color: black;
                    font-weight: bolder;
                }
                p {
                    color: #2EADED;
                }
            `}</style>
            <a href={props.productUrl}>
                <div className="productImgContainer">
                    <img src={props.urlImg} alt={props.productName} />
                </div>
                <h5>{props.productName}</h5>
            </a>
            <p>{props.productPrice}</p>
        </div>
    )
}

// 
// the big banner section before the newest post section
// contain a link displayed as an image
// 
// See /src/BigBannerItem.js for more info of data structure
function BigBanner(props) {
    let item = props.item;
    return (
        <>
            <style jsx>{`
                div {
                    margin-bottom: 100px;
                }
                img {
                    transition: 0.8s;
                    width: 100%;
                }
                a:hover img {
                    opacity: 0.5;

                }    
            `}</style>
            <div>
                <a target="blank" href={item.linkUrl}>
                    <img src={item.backgroundUrl} alt="Special Offer" />
                </a>
            </div>
        </>
    )
}

// 
// The newest post section on the home page. 
// Should contain 3,6 or 9 posts
//
// See /src/data/NewestPostsItems.js for more info of data structure
function NewestPosts(props) {
    let items = props.items,
        postItems = [];
    for (let item of items) {
        postItems.push(Post(item));
    }
    return (
        <div>
            <style jsx>{`
                div {
                    text-align: center;
                    margin-bottom: 100px;
                }    
                h1 {
                    font-weight: bold;
                }
                @media(max-width: 576px){  
                    h1{
                        font-size: 1.5em;
                    }
                }
            `}</style>
            <h1>BÀI VIẾT MỚI NHẤT</h1>
            <p>Những bài viết mới nhất từ Duy</p><br />
            <div className="row">
                {postItems}
            </div>
        </div>
    )
}
//
// A single post element on the home page
// Should contains a post links displayed as 
// an image together with a title
function Post(props) {
    return (
        <div key={props.id} className="col-12 col-sm-12 col-md-4">
            <style jsx>{`
                div {
                    margin-bottom: 30px;
                }
                img {
                    width: 100%;
                    margin-bottom: 20px;
                    transition: 0.8s;
                }
                p {
                    color: #BDBDBD;
                    text-align: left;
                    margin-top: 0px;
                }    
                h5 {
                    line-height: 1.3;
                    text-align: left;
                    color: black;
                    font-weight: bold;
                }
                a:hover {
                    text-decoration: none;
                }
                a:hover img{
                    box-shadow:
                        1px 1px #53a7ea,
                        2px 2px #53a7ea,
                        3px 3px #53a7ea;
                    -webkit-transform: translateX(-3px);
                    transform: translateX(-3px);
                }
                @media(max-width: 576px){  
                    .carousel-indicators{
                        display: none;
                    }
                }
            `}</style>
            <a href={props.postUrl}>
                <img src={props.postImgUrl} alt={props.postTitle} />
                <p>{props.postDate}</p>
                <h5>{props.postTitle}</h5>
            </a>
            <p>{props.postDescription}<br />
                <a href={props.postUrl}>Xem thêm>></a>
            </p>
        </div>
    )
}
//
// Sponsor section above the footer in the Home page
function SponsorSection(props) {
    let items = props.items,
        sponsorItems = [];

        for (let item of items) {
        sponsorItems.push(Sponsor(item));
    }
    return (
        <>
            <style jsx>{`
                div {
                    margin-bottom: 100px;
                }
            `}</style>
            <div className="row d-flex justify-content-between">
                {sponsorItems}
            </div>
        </>
    )
}

// A single sponsor element
function Sponsor(props) {
    return (
        <div key={props.id} >
            <style jsx>{`
                img {
                    width: 150px;
                    margin-bottom: 30px;
                }    
            `}</style>
            <a href={props.sponsorUrl}>
                <img src={props.sponsorImgUrl} />
            </a>
        </div>
    )
} 