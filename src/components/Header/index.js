const Header = (activePage) => (
    <>
        <style jsx>{`
        nav {
            background: #fff !important;
        }
        nav .nav-item {
            margin-right: 30px;
            font-size: 1em;
        }
        nav .nav-item .nav-link{
            color: #4F4F4F !important;
            font-weight: bolder;
        }
        nav .nav-item.active .nav-link{
            color: #2EADED !important;
            border-bottom: 2px solid #2EADED;
        }
        nav .dropdown:hover .dropdown-menu {
            display: block !important;
        }
        nav .dropdown:hover>.dropdown-menu {
            display: block;
        }
        nav .dropdown > .dropdown-toggle:active {
            pointer-events: none;
        }
        nav .dropdown > .dropdown-toggle::after {
            display: none;
        }
        @media screen and (min-width:768px) {
            .navbar-nav .dropdown:hover>.dropdown-menu {
              display:block;margin:0
            }
          }    
        `}
        </style>
        
        <nav className="navbar navbar-expand-lg navbar-light bg-light">
            <div className="container">
                <a href="/" className="navbar-brand"><img alt="icon" src="assets/img/logo_header.png" width="140px" /></a>
                <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>

                <div className="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul className="navbar-nav ml-auto smooth-scroll">
                        <li className="nav-item active">
                            <a className="nav-link" href="/">Trang chủ</a>
                        </li>

                        <li className="nav-item dropdown">
                            <a className="nav-link" href="/san-pham" id="navbarDropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                Sản phẩm
                            </a>
                            <div className="dropdown-menu" aria-labelledby="navbarDropdown">
                                <a className="dropdown-item" href="#">Action</a>
                                <a className="dropdown-item" href="#">Another action</a>
                                <div className="dropdown-divider"></div>
                                <a className="dropdown-item" href="#">Something else here</a>
                            </div>
                        </li>
                        <li className="nav-item dropdown">
                            <a className="nav-link" href="/bai-viet" id="navbarDropdown2" role="button" aria-haspopup="true" aria-expanded="false">
                                Bài viết
                            </a>
                            <div className="dropdown-menu" aria-labelledby="navbarDropdown2">
                                <a className="dropdown-item" href="#">Action</a>
                                <a className="dropdown-item" href="#">Another action</a>
                                <div className="dropdown-divider"></div>
                                <a className="dropdown-item" href="#">Something else here</a>
                            </div>
                        </li>
                        <li className="nav-item">
                            <a className="nav-link" href="#">Thành phần <span className="sr-only">(current)</span></a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    </>
);

export default Header;