import InstagramItems from '../../data/InstagramItems';

const FooterGlobalStyles = () => {
    return (
        <style jsx="true">{`
            #footer {
                background: #F2F2F2;
                padding-top: 30px;
            }
            .footer-item {
                padding-right: 40px;
            }
            
            .footer-heading {
                height: 100px;
                display: flex;
                align-items: flex-end;
            }
            
            .footer-content {
                margin-top: 30px;
                color: #828282;
                font-size: 0.8em;
                line-height: 1.3;
            }

            #rudias {
                text-align: right;
                margin: 0 !important;
                padding: 10px;
                color: #BDBDBD;
            }    
        `}</style>
    )
}

const Footer = () => (
    <>
        <FooterGlobalStyles />
        <div id="footer">
            <div className="container">
                <div className="row">
                    <FooterDescription />
                    <Contact />
                    <Subscribe />
                    <Instagram items={InstagramItems}/>
                </div>
                <hr />
                <p id="rudias">Made with love by <a target="blank" href="https://rudias.asia">Rudias</a></p>
            </div>
        </div>
    </>
);
export default Footer;

function FooterDescription() {
    return (
        <div className="col-12 col-sm-6 col-md-6 col-lg-3 footer-item">
            <style jsx>{`
                img {
                    max-width: 200px;
                }
            `}</style>
            <div className="footer-heading">
                <img src="./assets/img/logo_header.png" />
            </div>
            <div className="footer-content">
                <p>Chào bạn, đây là website phân tích thành phần hóa học của mỹ phẩm mà Duy và các đồng sự đã dày công xây đắp nên. Nếu bạn thấy website này hữu ích, đừng quên đồng hành cùng Duy ở các channel khác nha!</p>
            </div>
        </div>
    )
}

function Contact() {
    return (
        <div className="col-12 col-sm-6 col-md-6 col-lg-3 footer-item">
            <div className="footer-heading">
                <h6><b>LIÊN HỆ</b></h6>
            </div>
            <div className="footer-content">
                <p>Email: <a href="mailto:gsvuduy2412@gmail.com">gsvuduy2412@gmail.com</a></p>
                <p>Phone: <a href="tel:0902204439">0902204439</a></p>
            </div>
        </div>
    )
}

function Subscribe() {
    return (
        <div className="col-12 col-sm-6 col-md-6 col-lg-3 footer-item">
            <style jsx>{`
                form {
                    display:flex !important;
                        border: 1px solid #7ACDF6;
                    flex-direction:row !important;
                    }

                    input {
                        border: none;
                        flex-grow:2;
                        padding: 10px;
                    }

                    button {
                    /* Just a little styling to make it pretty */
                        background:white;
                        border: none;
                    }    
            `}</style>
            <div className="footer-heading">
                <h6><b>SUBSCRIBE</b></h6>
            </div>
            <div className="footer-content">
                <p>Nhận email thông báo bài viết mới của Duy tại đây nha</p>
                <form>
                    <label style={{ display: "none" }} className="sr-only" htmlFor="email">Name</label>
                    <input type="email" id="email" name="email"  placeholder="Email của bạn" />

                    <button type="submit">
                        <img src="/assets/img/icon_send.png" width="20px" height="20px" />
                    </button>
                </form>
            </div>
        </div>
    )
}
//
// Instagram images in the footer
function Instagram(props) {
    let items = props.items,
        instagramItems = [];
    
    for (let item of items){
        instagramItems.push(InstagramItem(item));
    }
    return (
        <div className="col-12 col-sm-6 col-md-6 col-lg-3 footer-item">
            <div className="footer-heading">
                <h6><b>INSTAGRAM</b></h6>
            </div>
            <div className="footer-content">
                <div className="row">
                    {instagramItems}
                </div>
            </div>
        </div>
    )
}
function InstagramItem(props){
    return (
        <div key={props.id} className="col-4">
            <style jsx>{`
                img {
                    margin: 15px 0px 0 0;
                }    
            `}</style>
            <a href={props.imgUrl}><img src={props.imgUrl}/></a>
        </div>
    )
}