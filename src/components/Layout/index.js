//
// This is the layout component, which wrap specific contents and
// apply some GLOBAL styles/actions
//

import Header from '../Header';
import Footer from '../Footer';
import StylesGlobal from './styles';

const Layout = props => {
    return (
    <>
        <StylesGlobal/>
        <Header/>
        {props.children}
        <GoUpButton/>
        <Footer/>
    </>
    )
}

function GoUpButton() {
    return (
        <a style={{position: "fixed", bottom: "20px", right: "20px", zIndex: "1000"}} href="#"><img src="/assets/img/arrow_up.png" width="40px" /></a>
    )
}

export default Layout;