import Layout from '../components/Layout';
import Home from '../screens/Home'

export default function Index() {
    return (
        <Layout>
            <Home/>
        </Layout>
    )
}