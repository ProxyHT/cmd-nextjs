export default [
    {
        id: 1,
        sponsorUrl: 'https://facebook.com',
        sponsorImgUrl: './assets/img/home_sponsor1.png'
    },
    {
        id: 2,
        sponsorUrl: 'https://facebook.com',
        sponsorImgUrl: './assets/img/home_sponsor2.png'
    },
    {
        id: 3,
        sponsorUrl: 'https://facebook.com',
        sponsorImgUrl: './assets/img/home_sponsor3.png'
    },
    {
        id: 4,
        sponsorUrl: 'https://facebook.com',
        sponsorImgUrl: './assets/img/home_sponsor4.png'
    },
    {
        id: 5,
        sponsorUrl: 'https://facebook.com',
        sponsorImgUrl: './assets/img/home_sponsor5.png'
    },
    {
        id: 6,
        sponsorUrl: 'https://facebook.com',
        sponsorImgUrl: './assets/img/home_sponsor6.png'
    },
]