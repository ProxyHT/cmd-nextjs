// 
// use in /screens/Home/content.js
//

export default [
    {
        id: 1,
        postTitle: "Da tay, chăm sóc sao cho đúng? Gợi ý từ CallmeDuy.",
        postDescription: "Thông thường chúng ta được hướng dẫn là phải dùng kem bôi tay cho tay. Tuy nhiên, bạn có biết là kem thoa mặt sử dụng cũng rất hiệu quả cho da tay không?",
        postDate: "14.02.2020",
        postUrl: "/posts/cham-soc-da-tay",
        postImgUrl: "/assets/img/home_blogimg.png"
    },
    {
        id: 2,
        postTitle: "Da tay, chăm sóc sao cho đúng? Gợi ý từ CallmeDuy.",
        postDescription: "Thông thường chúng ta được hướng dẫn là phải dùng kem bôi tay cho tay. Tuy nhiên, bạn có biết là kem thoa mặt sử dụng cũng rất hiệu quả cho da tay không?",
        postDate: "14.02.2020",
        postUrl: "/posts/cham-soc-da-tay",
        postImgUrl: "/assets/img/home_blogimg2.png"
    },
    {
        id: 3,
        postTitle: "Da tay, chăm sóc sao cho đúng? Gợi ý từ CallmeDuy.",
        postDescription: "Thông thường chúng ta được hướng dẫn là phải dùng kem bôi tay cho tay. Tuy nhiên, bạn có biết là kem thoa mặt sử dụng cũng rất hiệu quả cho da tay không?",
        postDate: "14.02.2020",
        postUrl: "/posts/cham-soc-da-tay",
        postImgUrl: "/assets/img/home_blogimg3.png"

    },
]